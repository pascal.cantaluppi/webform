# Webform Tutorial

This is a [Next.js](https://nextjs.org/) project with [React Hook Form](https://react-hook-form.com/)

<p>
<img src="https://gitlab.com/pascal.cantaluppi/webform/-/raw/master/public/img/react-hook-form.png" alt="react-hook-form" width="500" />
</p>

## Setup

### Projekt Template

Bootstrapped with `create-nect-app`

<p>
<img src="https://gitlab.com/pascal.cantaluppi/webform/-/raw/master/public/img/create-next-app.png" alt="create-next-app" width="400" />
</p>

### Getting Started

Install the dependencies

```bash
npm i
```

Run the development server

```bash
npm run dev
```

And open [http://localhost:3000](http://localhost:3000) with your browser.
