import { NextApiRequest, NextApiResponse } from "next";

import assert from "assert";
import shallowequal from "shallowequal";
import jwt from "jsonwebtoken";

const secret = process.env.SECRET;

const users: any = [
  {
    email: "pascal@cantaluppi.ch",
  },
  {
    email: "fred.flinstone@gmail.com",
  },
];

const handler = (req: NextApiRequest, res: NextApiResponse) => {
  if (!secret) {
    console.error("Forgot something like for example the .env?");
  } else if (req.method === "POST") {
    try {
      assert.notStrictEqual(null, req.body.email, "Email required");
      assert.notStrictEqual(null, req.body.password, "Password required");
    } catch (bodyError) {
      res.status(403).send(bodyError.message);
    }
    if (
      !users.some((item: any) =>
        shallowequal(item, {
          email: req.body.email,
        })
      )
    ) {
      res.status(404).json({ error: true, message: "User not found" });
      return;
    } else {
      if (req.body.password === "12345") {
        const token = jwt.sign({ email: req.body.email }, secret, {
          expiresIn: 3600, // 60 minutes
        });
        res.status(200).json({ token });
        return;
      } else {
        res.status(401).json({ error: true, message: "Authentication failed" });
      }
    }
  } else {
    // Handle any other HTTP method
    res.statusCode = 401;
  }
  res.end();
};

export default handler;
