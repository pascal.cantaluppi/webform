import { NextApiRequest, NextApiResponse } from "next";

import jwt from "jsonwebtoken";

const secret = process.env.SECRET;

const handler = (req: NextApiRequest, res: NextApiResponse) => {
  if (!secret) {
    console.error("Forgot something like for example the .env?");
  } else if (req.method === "GET") {
    if (!("authorization" in req.headers)) {
      res.status(401).json({ message: "Authorization header missing" });
      return;
    }
    let decoded;
    let token;
    if (req.headers.authorization) {
      token = req.headers.authorization.split(" ")[1];
    }
    if (token) {
      try {
        console.log("token: " + token);
        decoded = jwt.verify(token, secret);
        console.log("login: " + JSON.stringify(decoded));
      } catch (e) {
        //console.error(e);
        console.error(
          "JsonWebTokenError: secret or public key must be provided"
        );
      }
    }
    if (decoded) {
      res.json(decoded);
      return;
    } else {
      res.status(401).json({ message: "Authentication failed" });
    }
  }
  res.end();
};

export default handler;
