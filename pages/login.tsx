import React from "react";

import { NextPage } from "next/types";

import { useForm } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";

import "bootstrap/dist/css/bootstrap.min.css";

interface FormData {
  email: string;
  password: string;
}

const LoginPage: NextPage = () => {
  const { register, errors, handleSubmit } = useForm<FormData>();
  const onSubmit = handleSubmit((data) => {
    console.log(data);
  });

  return (
    <div className="container w-25 p-3">
      <div className="row mb-5">
        <div className="col-lg-12 text-center">
          {/* <h1 className="mt-5">React Hook Form</h1> */}
          <img
            src="/img/login.png"
            alt="Login"
            style={{
              marginTop: 10,
              marginBottom: 0,
            }}
          />
        </div>
      </div>
      <form onSubmit={onSubmit}>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input
            name="email"
            placeholder="Enter email"
            className={`form-control ${errors.email ? "is-invalid" : ""}`}
            ref={register({
              required: "Email is required",
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                message: "Invalid address format",
              },
            })}
          />
          <ErrorMessage
            className="invalid-feedback"
            name="email"
            as="div"
            errors={errors}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <input
            name="password"
            type="password"
            placeholder="Enter password"
            className={`form-control ${errors.password ? "is-invalid" : ""}`}
            ref={register({
              required: "Password is required",
            })}
          />
          <ErrorMessage
            className="invalid-feedback"
            name="password"
            as="div"
            errors={errors}
          />
        </div>
        <br />
        <button className="btn btn-dark btn-block" type="submit">
          Sign in
        </button>
      </form>
    </div>
  );
};

export default LoginPage;
