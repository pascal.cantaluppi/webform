// import React from "react";

// import { NextPage } from "next/types";
// import { useForm } from "react-hook-form";

// import Button from "@material-ui/core/Button";
// import Container from "@material-ui/core/Container";
// import Grid from "@material-ui/core/Grid";
// import TextField from "@material-ui/core/TextField";
// import { makeStyles } from "@material-ui/core/styles";

// const useStyles = makeStyles((theme) => ({
//   container: {
//     padding: theme.spacing(3),
//   },
// }));

// interface FormData {
//   email: string;
//   password: string;
// }

// const LoginPage: NextPage = () => {
//   const classes = useStyles();

//   const { handleSubmit, register } = useForm<FormData>();
//   const onSubmit = handleSubmit((data) => {
//     console.log(data);
//   });

//   return (
//     <Container className={classes.container} maxWidth="xs">
//       <img
//         src="/img/ipso_small.png"
//         alt="ipso! Bildung"
//         style={{
//           marginTop: 10,
//           marginBottom: 10,
//         }}
//       />
//       <form onSubmit={onSubmit}>
//         <Grid container spacing={3}>
//           <Grid item xs={12}>
//             <Grid container spacing={2}>
//               <Grid item xs={12}>
//                 <TextField
//                   fullWidth
//                   inputRef={register}
//                   label="Email"
//                   name="email"
//                   size="small"
//                   variant="outlined"
//                 />
//               </Grid>
//               <Grid item xs={12}>
//                 <TextField
//                   fullWidth
//                   inputRef={register}
//                   label="Password"
//                   name="password"
//                   size="small"
//                   type="password"
//                   variant="outlined"
//                 />
//               </Grid>
//             </Grid>
//           </Grid>
//           <Grid item xs={12}>
//             <Button
//               style={{
//                 color: "#ffffff",
//                 backgroundColor: "#009CC3",
//               }}
//               fullWidth
//               type="submit"
//               variant="contained"
//             >
//               Log in
//             </Button>
//           </Grid>
//         </Grid>
//       </form>
//     </Container>
//   );
// };

// export default LoginPage;
